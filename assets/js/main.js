// Ciclo de vida del item
const addTodo = (e) => {
  const target = $(e.target);

  if(e.keyCode !== 13 || target.val().length < 3 )
    return ;

  const listItem = $(formatingListItem(e.timeStamp, target.val()));

  $('#todo-list').append(listItem);
  $(document).trigger('updateItemsView');
  $(document).trigger('reviseFilter');
  target.val("");

  return ;
};
const formatingListItem = (id, activity) => (
  `
    <li id="list-item-${id}" class="shadow">
      <input type="checkbox" name="todo-item" >
      <p>${activity}</p>
      <input type="text" value="${activity}" class="hidden">
      <button>×</button>
    </li>
  `
);
const removeTodo = (e) => {
  $(e.target).parent().remove();
  $(document).trigger('updateItemsView');

  return ;
};
const editTodoItem = ({target}) => {
  const pElement = $(target),
        inputElement = pElement.siblings(".hidden");

  pElement.addClass("hidden");
  inputElement.removeClass("hidden");

  return ;
};
const updateTodoItem = (e) => {
  const target = $(e.target);

  if (e.keyCode !== 13 || target.val().length < 3)
    return;

  const pElement = target.siblings("p.hidden");

  pElement.html(target.val());
  target.addClass("hidden");
  pElement.removeClass("hidden");

  return ;
};
// Utilidades
const reviseTodoItems = (e) => {
  const itemsStatistics = getItemsStatistics();
  const headerButton = $('#todo-header button');
  const todoFooter = $('#todo-footer');
  

  if (itemsStatistics.total.count === 0) {
    headerButton.addClass("hidden");
    todoFooter.addClass("hidden")
  } else {
    headerButton.removeClass("hidden");
    todoFooter.removeClass("hidden");
  }

  updateCounter(itemsStatistics.left.count);
  $(document).trigger('reviseFilter');

  return;
};
const getItemsStatistics = () => {
  const $items           = $('ul#todo-list').children();
  const $itemsLeft       = $("input[type=checkbox]:not(:checked)");
  const $itemsCompleted  = $("input[type=checkbox]:checked");

  return {
    total:      {count: $items.length, $items},
    left:       {count: $itemsLeft.length, items: $itemsLeft},
    completed:  {count: $itemsCompleted.length, items: $itemsCompleted}
  };
};
const updateCounter = (itemsLeft = getItemsStatistics().left.count) => {
  const itemCounter = $('#todo-items-remaining span');

  itemCounter.html(itemsLeft || 0);

  return ;
};
const toggleTodos = () => {
  const $todoItems = $('#todo-list input[type=checkbox]');
  const itemsLeft = $todoItems.filter(":not(:checked)").length;

  itemsLeft ? checkItems($todoItems) : uncheckItems($todoItems);

  updateCounter();
  $(document).trigger('reviseFilter');

  return ;
};
const uncheckItems = $items => {
  $items.each((_, item) => {
    item.checked = false;
  });

  return ;
};
const checkItems = $items => {
  $items.each((_, item) => {
    item.checked = true;
  });

  return ;
};
const updateTodoCounter = () => {
  updateCounter();
  $(document).trigger('reviseFilter');

  return ;
};
const updateFilters = () => {
  const $currentFiler = $('#todo-footer .actions p.active')

  filterTodoItems({target: $currentFiler});

  return;
}
const filterTodoItems = ({target}) => {
  const $target = $(target);
  const filterType = $target.attr("data-filter");

  $target.addClass("active");
  $target.siblings().removeClass("active");
  filterFunctions(filterType);
  
  return ;
};
const filterFunctions = (filterType) => {
  const filters = {
    items:          $('ul#todo-list li'),
    activeItems:    $('ul#todo-list li input[type=checkbox]:not(:checked)'),
    completedItems: $('ul#todo-list li input[type=checkbox]:checked'),
    all: function() {
      this.items.each((_, item) => ($(item).removeClass("hidden")));
    },
    active: function() {
      this.activeItems.each((_, item) => ($(item).parent().removeClass("hidden")));
      this.completedItems.each((_, item) => ($(item).parent().addClass("hidden")));
    },
    completed: function() {
      this.activeItems.each((_, item) => ($(item).parent().addClass("hidden")));
      this.completedItems.each((_, item) => ($(item).parent().removeClass("hidden")));
    }
  };

  filters[filterType]();

  return;
};
$(document).ready(function() {
  $(document).on('keyup.todos', "input#new-todo", addTodo);
  $('#todo-container').on('click.todos', "#todo-list button", null, removeTodo);
  $('#todo-container').on('click.todos', "#todo-header button", null, toggleTodos);
  $(document).on('updateItemsView.todos ', reviseTodoItems);
  $(document).on('dblclick.todos', "ul#todo-list li p", editTodoItem);
  $(document).on('keyup.todo', "ul#todo-list li input[type=text]", updateTodoItem);
  $(document).on('click.todos', "ul#todo-list input[type=checkbox]", updateTodoCounter);
  $(document).on('click.todos', "#todo-footer .actions p", filterTodoItems);
  $(document).on('reviseFilter.todos', updateFilters);
});